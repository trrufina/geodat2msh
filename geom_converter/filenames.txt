'geodat/U_40_40.dat'
'vtx/U_.vtx'
6
0.01

# program takes 3 parameters from this file
# 1, 2 - strings in '', 3 - integer, 4 - real
# 1 - input geodat file
# 2 - output msh file
# 3 - precision (number of digits after comma) in coords of geodat 
# 4 - Gkf: cord_in_meters = cord_in_file * Gkf 
# if geodat coords in cm, Gkf = 0.01, coords in mm Gkf = 0.001