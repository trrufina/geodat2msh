Program geodat_to_vtx 
    use Geom_Formats
    implicit none
    Type(tGeodat)  :: Grid
	character(len = 24) :: fGeodat, fout
	real*8 eps
		
	open(10, file='filenames.txt')
	read(10,*) fGeodat
	read(10,*) fout 
	read(10,*) Grid%D
	read(10,*) Grid%Gkf 
	close(10)	
	print *, 'Read Geodat file: ', fGeodat
	print *, 'Write to VTX file: ', fout
	
	call Grid%read_geodat(fGeodat) 
	call Grid%alloc_node_elem
	
	eps = 10.0**(-Grid%D) * sqrt(3.0)
	print *, 'Distance considered zero =', eps 
	call Grid%get_node_elem(eps)

	call write_vtx_1mod(Grid, fout)
	
	call Grid%free_geodat

end program