Module Geom_Formats
   Implicit none
!*------------------geodat format--------------------*
!	n_obj 
!   k_mod(n)         	      		  \
! 	nzam(k)					\	       | foreach						
!   j_mod(k)				| foreach  | object	
!   ni, nj (k)				|  module  | 
!   (x,y,z) x 4 x j_mod(k)	/ 		  /	
!*---------------------------------------------------*
    Type :: tGeodat 
		Integer                        	:: n_obj  ! number of objects
		Integer                         :: n_mod  ! number of modules on all objects
		Integer                         :: jmax   ! summary number of frames
		Integer,Pointer,Dimension(:)   	:: jobj   ! (n_obj) 	num of vortex on each object
		Integer,Pointer,Dimension(:)   	:: k_mod  ! (n_obj) 	num of modules on each object
		Integer,Pointer,Dimension(:)   	:: j_mod  ! (n_mod) 	num of vortex on each module
		Integer,Pointer,Dimension(:)   	:: o_mod  ! (n_mod) 	num of object for this module
		Integer,Pointer,Dimension(:)   	:: zam_mod ! (n_mod) 	closure sign of each module (priznak zamknutosti)
		Integer,Pointer,Dimension(:,:)  :: begend ! (2,n_mod) 	num of first and last frame on each module
		Integer,Pointer,Dimension(:,:) 	:: pt_mod ! (2, n_mod)	ni, nj for each module, ni(k) * nj(k) = j_mod(k)
		real*8,Pointer,Dimension(:,:,:) :: rut 	  ! (3,4,jmax)	geometry of each frame, 3-4 points  
		! non-geodat data
		Integer                        	:: n_node 	! number of nodes (3D-points)
		Integer                        	:: n_max	! maximal number of nodes (4 * n_elem)
		Integer                         :: n_elem  	! number of elements (frames, Triangle or Quadrangle )
		Integer,Pointer,Dimension(:,:)	:: elems 	! (4,n_elem)	indices of nodes composing element (in array 'nodes') 
		real*8,Pointer,Dimension(:,:)	:: nodes 	! (3,n_node)	x,y,z-coordinates of nodes  
		! extra parameters
		Integer                         :: M 		! number of digits before comma in node coordinates
		Integer                         :: D	  	! number of digits after comma in node coordinates
		real*8							:: Gkf		! scale coef: cord_in_meters = cord_in_file * Gkf 		
		
		contains
			procedure, pass :: read_geodat
			procedure, pass :: free_geodat
			procedure, pass :: alloc_node_elem
			procedure, pass :: get_node_elem
			procedure, pass :: get_format
			procedure, pass :: write_msh_mods
    End Type tGeodat 
	
	contains 
	
	subroutine read_geodat(Grid, filename)
		class(tGeodat) :: Grid
		integer i, ii, iii, k, kmd, kmdmax, nob, jmax, jmaxc
		integer kc, jc, nr, tmod
		real*8 a(12)
		character(len = *) :: filename 

		open(10,file=filename,status='old')
		read(10,*) Grid%n_obj  
		jmax = 0
		kmdmax = 0
		do nob = 1, Grid%n_obj
            read(10,*) kmd
			kmdmax = kmdmax + kmd
			do k = 1, kmd
				read(10,*) iii
				read(10,*) jmaxc
				jmax = jmax + jmaxc
				read(10,*) ii, iii
				do i = 1, jmaxc
					read(10,*) a
				end do
			end do
		end do
		Grid%jmax = jmax
		Grid%n_mod = kmdmax
				
		allocate( Grid%jobj ( Grid%n_obj ) )
		allocate( Grid%k_mod ( Grid%n_obj ) )
		allocate( Grid%j_mod ( Grid%n_mod ) )
		allocate( Grid%o_mod ( Grid%n_mod ) )
		allocate( Grid%zam_mod ( Grid%n_mod ) )
		allocate( Grid%begend ( 2, Grid%n_mod ) )
		allocate( Grid%pt_mod ( 2, Grid%n_mod ) )
		allocate( Grid%rut ( 3, 4, Grid%jmax ) )

		rewind(10)
		
		jmaxc = 0
		nr = 0
		kc = 0
		Grid%jobj = 0
		read(10,*) ii ! read n_obj again
		do nob = 1, Grid%n_obj
            read(10,*) Grid%k_mod(nob) ! read number of modules on cur object
			do k = 1, Grid%k_mod(nob)
				tmod = k+kc ! current module number 
				read(10,*)  Grid%zam_mod(tmod) ! nzamtel 
				read(10,*) jc
				Grid%jobj(nob) = Grid%jobj(nob) + jc
				Grid%j_mod(tmod) = jc
				Grid%o_mod(tmod) = nob
				Grid%begend(1,tmod) = jmaxc+1  
		        jmaxc = jmaxc + jc
				Grid%begend(2,tmod) = jmaxc
				read(10,*) Grid%pt_mod(1,tmod), Grid%pt_mod(2,tmod) ! ni, nj		
				do i = 1, jc
					nr = nr + 1
					read(10,*) Grid%rut(1,1,nr),Grid%rut(2,1,nr),Grid%rut(3,1,nr),&
							Grid%rut(1,2,nr),Grid%rut(2,2,nr),Grid%rut(3,2,nr),&	
                            Grid%rut(1,3,nr),Grid%rut(2,3,nr),Grid%rut(3,3,nr),&
                            Grid%rut(1,4,nr),Grid%rut(2,4,nr),Grid%rut(3,4,nr)
				end do
			end do
			kc = kc + Grid%k_mod(nob)
		end do
		
		close (10)
			
	end subroutine read_geodat

	subroutine free_geodat(Grid)
		class(tGeodat) :: Grid
		deallocate( Grid%jobj, Grid%k_mod, Grid%j_mod, Grid%o_mod, Grid%zam_mod, Grid%begend, Grid%pt_mod )
		deallocate( Grid%rut, Grid%elems, Grid%nodes  )
	end subroutine free_geodat
	
	subroutine alloc_node_elem(Grid)
		class(tGeodat) :: Grid	
		Grid%n_elem = Grid%jmax 
		Grid%n_max = Grid%jmax  * 4
		allocate( Grid%elems (4, Grid%n_elem) )
		allocate( Grid%nodes (3, Grid%n_max) )
	end subroutine alloc_node_elem
	
	subroutine get_node_elem(Grid, eps)
		!Parse rut: find unique nodes and number them, set elems
		class(tGeodat) :: Grid
		integer i_elem, j_elem, i_node, act_node
		real*8 x(3), rho, eps, hmin
		
		hmin = 1.0d10
		Grid%n_node = 0

		do i_elem = 1, Grid%jmax
			do j_elem = 1, 4
				x = Grid%rut(:,j_elem,i_elem) 
				act_node = -1
				do i_node = 1, Grid%n_node
					call dstabs(x, Grid%nodes(:,i_node), rho)
					if (rho .lt. eps) then 
						act_node = i_node
						exit 
					else 
						if (rho .lt. hmin) hmin = rho 
					end if 
				end do 
				if (act_node .eq. -1) then 
					Grid%n_node = Grid%n_node + 1
					act_node = Grid%n_node
					Grid%nodes(:, act_node) = x
				end if 
				Grid%elems(j_elem, i_elem) = act_node
			end do 
		end do
				
		print *, 'Minimal nonzero distance =', hmin 
		print *, 'Number of points =', Grid%n_node
		print *, 'Number of frames =', Grid%n_elem 
		
		! scale node coordinates 
		Grid%nodes = Grid%nodes * Grid%Gkf 
		call Grid%get_format()

	end subroutine get_node_elem
	
	! get number of digits before and after comma in decimal form 
	subroutine get_format(Grid)
		Class(tGeodat) :: Grid
		real*8 maxpt
		
		maxpt = maxval(abs(Grid%nodes))		
		Grid%M = floor(log10(maxpt)+1) 
		Grid%M = max(Grid%M, 1)
		
		Grid%D = Grid%D - nint(log10(Grid%Gkf))
		print *, 'Format =', Grid%M, Grid%D
		
	end subroutine get_format

	subroutine write_msh_mods(Grid, filename)
		!Save MSH data in ANSIS format, multiple module
		Class(tGeodat) :: Grid
		character(len = *) :: filename 
		character(len = 20) :: fmtstr, ws, ds
		integer i, imd, W
		
		open(10,file=filename)
		write(10,'(A,/,A,/,A)') '(0 " Created by : geodat to msh transformer ")', '(2 3)', '(0 "Node Section")'
		
		print '(A,I0,A,I0)', 'write format: ', Grid%M, '.', Grid%D
		W = Grid%M + Grid%D + 3
		write(ws, '(I0)') W 
		write(ds, '(I0)') Grid%D 
		fmtstr = '(3F'//trim(ws)//'.'//trim(ds)//')'

		! save nodes 
		write(10,'(A, Z0, A)') '(10 (0 1 ', Grid%n_node,' 0 3))'
		write(10,'(A, Z0, A,/,A)') '(10 (3 1 ', Grid%n_node,' 0 3)', '('
		do i = 1, Grid%n_node
			write(10,  fmtstr) Grid%nodes(:,i) 
		end do 
		write(10,'(A)') '))'

		! save elements 
		write(10,'(A, Z0, A)') '(13 (0 1 ', Grid%n_elem,' 0 0))'

		do imd = 1, Grid%n_mod
			write(ws, '(A,I0)') 'MOD', imd 
			write(10,'(A,A,A)') '(0 "Faces of zone ', trim(ws), '")'
			write(10,'(A, Z0,X,Z0, A)') '(13 (4 ', Grid%begend(1,imd), Grid%begend(2,imd), ' 3 4)(' ! last 4 - number of sides in element (must be 4)
			do i = Grid%begend(1,imd), Grid%begend(2,imd)
				write(10, '(4(Z0,X),A)' ) Grid%elems(:,i), '0 0'
			end do 
			write(10,'(A,/,A)') ')', ')'
		end do 
		
		write(10,'(A)') '(0 "Zone Sections")'
		do imd = 1, Grid%n_mod
			write(ws, '(A,I0)') 'MOD', imd 
			write(10,'(A,A,A)') '(39 (4 wall ', trim(ws),')())'
		end do 
			
		close(10)
	end	subroutine write_msh_mods
	
	subroutine write_vtx_1mod(Grid, filename)
		Class(tGeodat) :: Grid
		character(len = *) :: filename 
		character(len = 20) :: fmtstr, ws, ds
		integer i, W, ist, ifin, j
	
		open(10,file=filename)
		! write preamble 
		write(10,'(A,/,A,/,A,/,A)'),'<types>','type O01SET01','ObjectID :: Integer','OuterDomain :: Integer'
		write(10,'(A,/,A,/,A,/,A)'),'InnerDomain :: Integer','SurfaceType :: Integer','Cells :: Polygon','end type'
		write(10,'(A,/,A,/,A,/,A)'),'type ObjectFeatures1','ObjectID :: Integer','DielDomNum :: Integer','DomNum :: Integer'
		write(10,'(A,/,A,/,A,/,A)'),'EpsRe001 :: Single','EpsIm001 :: Single','MuRe001 :: Single','MuIm001 :: Single'
		write(10,'(A,/,A,/,A)'),'end type','<end types>','<points>'

		! write points 
		W = Grid%M + Grid%D + 5
		write(ws, '(I0)') W 
		write(ds, '(I0)') Grid%D 
		fmtstr = '(3F'//trim(ws)//'.'//trim(ds)//')'	
		do i = 1, Grid%n_node
			write(10,  fmtstr) Grid%nodes(:,i) 
		end do 
		
		! write polygons
		write(10,'(A,/,A,/,A,/,A)'),'<end points>','<lines>','<end lines>','<polygons>'
		do i = 1, Grid%n_elem
			write(10, '(4(X,I0))' ) Grid%elems(:,i)
		end do 
	
		! write O01SET01
		write(10,'(A,/,A,/,A)'),'<end polygons>','<groups>','group O01SET01'
		write(10,'(I3,/,I3,/,I3,/,I3,/,A)'),1,1,1,1,'{'
		
		W = Grid%n_elem / 10 

		do i = 1, W 
			ist = 10*(i-1) + 1; ifin = 10 * i  
			write(10,*) (j, j=ist,ifin)
		end do 
		i = mod(Grid%n_elem,10)
		if (W > 0) then 
			ist = W*10 + 1; ifin = Grid%n_elem
			write(10,*) (j, j=ist,ifin)
		end if 
		
		! write ObjectFeatures1
		write(10,'(A,/,A,/,A)'),'}','end group','group ObjectFeatures1'
		write(10,'(I3,/,I3,/,I3,/,I3,/,I3,/,I3,/,I3)'),1,1,2,1,0,1,0
		write(10,'(A,/,A)'),'end group','<end groups>'
		close(10)
	
  end subroutine 
	
	subroutine dstabs(x, y, r_abs)
	  	! Calc r_abs = |x - y| for given x and y
		real*8 x(3), y(3), r(3), r_abs
		r = x - y
		r_abs = sqrt(r(1)**2 + r(2)**2 + r(3)**2)
	end subroutine dstabs
	
End Module Geom_Formats